﻿using UnityEngine;

public class SaveManager : MonoBehaviour 
{
    public static SaveState state;

    private void Awake()
    {
        GetState();
    }

    private void GetState()
    {
        if (PlayerPrefs.HasKey("Save"))
        {
            state = PlayerPrefs.GetString("Save").Deserialize<SaveState>();
        }
        else
        {
            state = new SaveState();
        }
    }
}
