﻿public class SaveState
{
    public bool[] acquiredCats;
    public bool[] acquiredTies;
    public bool[] acquiredHats;

    public int appleCount;
    public int pineappleCount;

    public int selectedCat;
    public int selectedHat;
    public int selectedTie;
}
