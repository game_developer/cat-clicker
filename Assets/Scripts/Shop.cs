﻿using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    [Header("Goods")]
    public GameObject cats;
    public GameObject hats;
    public GameObject foods;

    [Space]
    public GameData gameData;

    private void Awake()
    {
        InitShop();
    }

    private void Start()
    {
        GameObject.FindObjectOfType<GM>().ShopHidden += ResetSelections;
    }

    private void InitShop()
    {
        //SET PRICES
        foreach (Transform child in transform)
        {
            if (child.tag == "Goods")
            {
                int index = 0;
                foreach (Transform grandChild in child)
                {
                    grandChild.Find("Price").GetComponent<Text>().text = GetPrice(grandChild.tag, index).ToString();
                    index++;
                }
            }
        }
    }

    private int GetPrice(string tag, int index)
    {
        switch (tag)
        {
            case "Cat": return gameData.catPrices[index];
            case "Hat": return gameData.hatPrices[index];
            case "Food": return gameData.foodPrices[index];
            default: throw new UnityException(tag);
        }
    }

    public void OnCatsClick()
    {
        ToggleFields(setActive: false);
        cats.SetActive(true);
    }

    public void OnHatsClick()
    {
        ToggleFields(setActive: false);
        hats.SetActive(true);
    }

    public void OnFoodsClick()
    {
        ToggleFields(setActive: false);
        foods.SetActive(true);
    }

    public void ResetSelections()
    {
        ToggleFields(setActive: true);
        ToggleGoods(setActive: false);
    }

    public void OnGoodClick(GameObject good, int index)
    {
        int price = System.Convert.ToInt32(good.transform.Find("Price").GetComponent<Text>().text);

        if (price < GM.currentScore)
        {
            GM.currentScore -= price;
        }

        else
        {
            Debug.Log("Cannot buy this item, no funds");
        }
    }

    private void ToggleGoods(bool setActive)
    {
        foreach (Transform child in transform)
        {
            if (child.tag == "Goods")
            {
                child.gameObject.SetActive(setActive);
            }
        }
    }

    private void ToggleFields(bool setActive)
    {
        foreach (Transform child in transform)
        {
            if (child.tag == "Field")
            {
                child.gameObject.SetActive(setActive);
            }
        }
    }
}
