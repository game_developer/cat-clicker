﻿using UnityEngine;

[CreateAssetMenu]
public class GameData : ScriptableObject 
{
    public int[] catPrices;
    public int[] hatPrices;
    public int[] foodPrices;
}
