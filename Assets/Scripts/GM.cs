﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GM : MonoBehaviour
{
    #region Variables
    [Header("Objects")]
    public Text scoreText;
    public RectTransform shop;
    public GameObject shopPanelCloseButton;
    public Animator cat;

    [HideInInspector]
    public static int currentScore = 0;

    private Vector2 beginDrag;
    private Vector2 endDrag;
    private const float minMagnitude = 100f;

    private bool shopIsMoving = false;
    private float panelMoveSpeed = 20f;
    #endregion

    public event System.Action ShopHidden;
    public virtual void OnShopHidden()
    {
        if (ShopHidden != null)
        {
            ShopHidden();
        }
    }

    private void Start()
    {
        shopPanelCloseButton.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            beginDrag = Input.mousePosition;

        }
        else if (Input.GetMouseButtonUp(0))
        {
            endDrag = Input.mousePosition;

            Vector2 direction = endDrag - beginDrag;

            if (direction.y > minMagnitude)
            {
                StartCoroutine(BringPanel(isUp: true));
            }

            else if (direction.y < -minMagnitude)
            {
                StartCoroutine(BringPanel(isUp: false));
            }
        }

        else if (!Input.GetMouseButton(0))
        {
            beginDrag = endDrag = Vector2.zero;
        }
    }

    IEnumerator BringPanel(bool isUp = true)
    {
        if (!shopIsMoving)
        {
            if (isUp)
            {
                shopPanelCloseButton.SetActive(true);
            }
            else
            {
                shopPanelCloseButton.SetActive(false);
            }

            shopIsMoving = true;
            Vector3 endPos = isUp ? Vector3.zero : Vector3.down * 150;

            float approximateFactor = isUp ? -0.5f : -149.5f;

            while ((isUp ? shop.anchoredPosition3D.y : -shop.anchoredPosition.y) < (isUp ? approximateFactor : -approximateFactor))
            {
                shop.anchoredPosition3D = Vector3.Lerp(shop.anchoredPosition3D, endPos, panelMoveSpeed * Time.deltaTime);
                yield return null;
            }

            shopIsMoving = false;

            if (isUp == false)
            {
                OnShopHidden();
            }
        }
    }

    public void OnCloseClick()
    {
        StartCoroutine(BringPanel(isUp: false));
    }

    public void OnCatClick()
    {
        currentScore++;
        scoreText.text = currentScore.ToString().PadLeft(7, '0');

        cat.SetTrigger("Click");
    }
}
